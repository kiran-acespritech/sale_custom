# -*- encoding: utf-8 -*-
##############################################################################
#    Copyright (c) 2012 - Present Acespritech Solutions Pvt. Ltd. All Rights Reserved
#    Author: <info@acespritech.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    A copy of the GNU General Public License is available at:
#    <http://www.gnu.org/licenses/gpl.html>.
#
##############################################################################
from openerp import models, fields, api, _
from collections import defaultdict
from odoo.exceptions import Warning

class sale_order(models.Model):
    _inherit = "sale.order"

    @api.one 
    @api.depends('order_line.price_subtotal', 'account_tax_ids.amount', 'order_line.tax_id')
    def _compute_amount(self):
    	tax_list = []
    	lst = []
    	for each in self.order_line:
            amt = 0
            for tax_line in each.tax_id:
                if not tax_line.children_tax_ids:
                    amt = ((each.price_subtotal) * tax_line.amount / 100)
                    tax_list.append({'amount': amt,
                                     'tax_id': tax_line.id})
                for child_tax in tax_line.children_tax_ids:
                    amt = ((each.price_subtotal) * child_tax.amount / 100)
                    tax_list.append({'amount': amt,
                                     'tax_id': child_tax.id})
        c = defaultdict(int)
        for d in tax_list:
        	c[d['tax_id']] += d['amount']
        result = dict(c)
        lst_ids = []
        for key, value in result.items(): 
            tax_id = self.env['account.tax'].browse(key)
            val = {'account_id':tax_id.account_id.id,
                   'amount':value,
                   'name':tax_id.name, 'tax_id':tax_id.id,
                   'currency_id':self.currency_id.id,
                   'sale_id': self.id}
            lst.append((0, 0, val))
        self.account_tax_ids = lst

    start_date = fields.Datetime('Schedule Date', required=True,
                              readonly=True,
                              states={'draft': [('readonly', False)]})
    end_date = fields.Datetime('Schedule End Date', required=True,
                              readonly=True,
                              states={'draft': [('readonly', False)]})
    desc = fields.Text('Description')
    deposite_amount = fields.Float('Deposite Amount',
                              readonly=True,
                              states={'draft': [('readonly', False)]},
                              copy=False)
    account_tax_ids = fields.One2many('sale.order.tax', 'sale_id', string="Tax line Ids", compute="_compute_amount", store=True)
    paypal_payment_url = fields.Char('Paypal Link', copy=False)

    @api.multi
    def action_quotation_send(self):
        result = super(sale_order, self).action_quotation_send()
        for paypal in self:
            if paypal.deposite_amount <= 0:
                paypal.paypal_payment_url = False
                return result
            acquirers = self.env['payment.acquirer'].search([('provider', '=', 'paypal'), ('company_id', '=', paypal.company_id.id)])
            if not acquirers:
                raise UserError(_('Goto apps, First, configure paypal payment acquirer.'))
            acquirer_url = acquirers.paypal_get_form_action_url()
            paypal.paypal_payment_url = ''.join([acquirer_url, '&cmd=_xclick&business=', acquirers.paypal_email_account, '&currency_code=', paypal.company_id.currency_id.name, "&amount=", str(paypal.amount_total), "&item_name=", paypal.name])
        return result

    @api.depends('order_line.price_total', 'deposite_amount')
    def _amount_all(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax - order.deposite_amount,
            })

    @api.model
    def create(self, vals):
        if vals.get('end_date') < vals.get('start_date'):
                raise Warning(_('End date should be greater than Start date.'))
        order_id = super(sale_order, self).create(vals)
        acquirers = self.env['payment.acquirer'].search([('provider', '=', 'paypal'), ('company_id', '=', order_id.company_id.id)])
        if order_id:
            if order_id.deposite_amount <= 0:
                return order_id
            if not acquirers:
                raise Warning(_('Goto apps, First, configure paypal payment acquirer.'))
            acquirer_url = acquirers.paypal_get_form_action_url()
            paypal_payment_url = ''.join([acquirer_url, '&cmd=_xclick&business=', acquirers.paypal_email_account, '&currency_code=', order_id.company_id.currency_id.name, "&amount=", str(order_id.amount_total), "&item_name=", order_id.name])
            order_id.write({'paypal_payment_url':paypal_payment_url})
        return order_id

    @api.multi
    def write(self, vals):
        res = super(sale_order, self).write(vals)
        for order in self:
            if order.end_date < order.start_date:
                raise Warning(_('End date should be greater than Start date.'))
            acquirers = self.env['payment.acquirer'].search([('provider', '=', 'paypal'), ('company_id', '=', order.company_id.id)])
            if not acquirers:
                raise Warning(_('Goto apps, First, configure paypal payment acquirer.'))
            if vals and vals.get('deposite_amount'):
                if vals.get('deposite_amount') <= 0:
                    return res
                else:
                    acquirer_url = acquirers.paypal_get_form_action_url()
                    paypal_payment_url = ''.join([acquirer_url, '&cmd=_xclick&business=', acquirers.paypal_email_account, '&currency_code=', order.company_id.currency_id.name, "&amount=", str(order.amount_total), "&item_name=", order.name])
                    order.write({'paypal_payment_url':paypal_payment_url})
        return res

    @api.multi
    def _prepare_invoice(self):
        res = super(sale_order, self)._prepare_invoice()
        one_bool = []
        res.update({'start_date': self.start_date,
                   'end_date': self.end_date,
                   'desc': self.desc,
                   'deposite_amount': self.deposite_amount})
        return res


class Sale_order_tax(models.Model):
    _name = "sale.order.tax"
    _description = "Sale Tax"
    _order = 'sequence'

    sale_id = fields.Many2one('sale.order', string='Sale', ondelete='cascade', index=True)
    name = fields.Char(string='Tax Description', required=True)
    tax_id = fields.Many2one('account.tax', string='Tax', ondelete='restrict')
    account_id = fields.Many2one('account.account', string='Tax Account', required=True, domain=[('deprecated', '=', False)])
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic account')
    amount = fields.Monetary()
    manual = fields.Boolean(default=True)
    sequence = fields.Integer(help="Gives the sequence order when displaying a list of invoice tax.")
    company_id = fields.Many2one('res.company', string='Company', related='account_id.company_id', store=True, readonly=True)
    currency_id = fields.Many2one('res.currency', related='sale_id.currency_id', store=True, readonly=True)
    base = fields.Monetary(string='Base', compute='_compute_base_amount')


class account_invoice(models.Model):
    _inherit = "account.invoice"


    @api.multi
    @api.depends('deposite_amount')
    def _compute_amount_total(self):
    
        self.amount_total = self.amount_tax + self.amount_untaxed - self.deposite_amount
        self.residual = self.amount_total

    start_date = fields.Datetime('Schedule Date')
    end_date = fields.Datetime('Schedule End Date')
    desc = fields.Text('Description')
    deposite_amount = fields.Float('Deposite Amount')
    amount_total = fields.Float(string="Total", store=True, readonly=True, compute="_compute_amount_total")
    residual = fields.Float(string="Amount Due", store=True, readonly=True, compute="_compute_amount_total")

    @api.one
    @api.depends('deposite_amount')
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_tax = sum(line.amount for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax - self.deposite_amount
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
